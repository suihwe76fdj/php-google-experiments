<?php

spl_autoload_register(function ($className) {
    if ($className[0] == '\\') {
        $className = substr($className, 1);
    }

    if (strpos($className, 'UnitedPrototype\\GoogleExperiments') !== 0) return;

    $classPath = strtr(substr($className, strlen('UnitedPrototype')), '\\', '/') . '.php';

    if (file_exists(__DIR__ . $classPath)) {
        require(__DIR__ . $classPath);
    }
});
